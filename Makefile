SHELL=/bin/bash
check:
	@set -e ;\
	find -type f -iname ympbuild | while read line ; do \
	    echo "Check $line" ;\
	    bash -n $line ;\
	done

git:
	git add .
	git commit
	git push