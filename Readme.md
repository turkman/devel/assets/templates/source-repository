# Source Repository
ympbuild file repository

## How to add ympbuild
1. create new ympbuild with **ymp template** command
2. run **make** for syntax check
3. if you use git run **make git**

## How to create git based package
1. create ympbuild and put into main directory
2. run **make** for syntax check
3. if you use git run **make git**
4. you can instal your package with:
```shell
ymp build --install git://example.org/package
# or
ymp build --install https://example.org/package.git
```
